

    class Card {
      constructor(name, email, title, body, id) {
        this.name = name; //users
        this.email = email; //users
        this.title = title; //post
        this.body = body; //post
        this.id = id;//post
          

       
      }
      createElement(element) {
        document.querySelector(".card-wrapper").insertAdjacentHTML(
          "afterbegin",
          ` <div data-id="${this.id}" class="card-container">
          <div><img class="card-img" src="./img/web_design3.jpg" alt="foto" width="70" height="70"></div>
            <div class="card-info">
            <span class="card-name">${this.name}</span>
            <i  class="fa-solid fa-cloud cloud"></i>
            <a class="card-link" href="#">${this.email} 3ч</a>

            
                <h3 class="card-title"> <i class="fa-regular fa-flag flag"></i> ${this.title}</h3>
             <p class="card-text"> ${this.body}</p>
            <div class="card-icons">
                <i class="fa-regular fa-comment"> <span class="icon-count">284</span></i>
                <i class="fa-regular fa-heart"> <span class="icon-count">988</span></i>
                <i class="fa-solid fa-retweet"> <span class="icon-count">10.5тис.</span></i>
                <i class="fa-solid fa-arrow-up-from-bracket"></i>
            </div>
            </div>
            <button class="card-btn" onclick="deletePost(${this.id})">Delete</button>
        </div> 
        `
        );
       
      }
    }

         


let urls = [
  "https://ajax.test-danit.com/api/json/users",
  "https://ajax.test-danit.com/api/json/posts",
];



let requests = urls.map((url) => fetch(url));

Promise.all(requests)
    .then((responses) =>
        Promise.all(responses.map((res) => res.json()))
    )
    .then(([users, posts]) => 
      posts.forEach((post) => {
            users.forEach((user) => { 
                if (user.id === post.userId) {
                  let postMen = new Card(
                    `${user.name}`,
                    `${user.email}`,
                    `${post.title}`,
                    `${post.body}`,
                    `${post.id}`
                  );
                  postMen.createElement();

        
                }
            })
            

        
           
    })
);


  

 
const deletePost = (id) => {

    fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
      method: "DELETE",
    })
        .then(response => {console.log(response)
    
    
        let container = document.querySelector(`[data-id='${id}']`);
        container.remove();
    
      })
      .catch((error) => {
        console.error("Помилка видалення", error);
      });
};


